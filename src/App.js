import {Component} from "react";


import Modal from "./components/modal/Modal";
import getProduct from "./services/GetProduct";
import Header from "./components/header/Header";
import ProductList from "./components/productList/ProductList";

import modalWindowDeclarations from './services/modalWindowDeclarations'
import './App.scss';



class App extends Component {
    constructor() {
        super();
    }
    state = {
        productInfo: [],
        loading: true,
        showModal: false,
        activeModal: {},
        activeModalCardId: null,
        starItems: [],
        bucketItems: [],

    }
    product = new getProduct()


    componentDidMount() {
        this.getProduct()
        let starItems = localStorage.getItem('starItems')
        if (starItems !== null){
            this.setState(prevState => ({starItems : JSON.parse(starItems)}))
        }
        let bucketItems = localStorage.getItem('bucketItems')
        if (bucketItems !== null){
            this.setState(prevState => ({bucketItems : JSON.parse(bucketItems)}))
        }
    }
    getModalInfo = () => {
        return modalWindowDeclarations.map(item => item)
    }

    getProduct = async () => {
        const productInfo = await this.product.getProductInfo();
        this.setState({
            productInfo: [...productInfo],
            loading: false
        });
    }

    handleStarClick = (id) =>{
        const { starItems } = this.state;
        if (starItems.includes(id)) {
            const newStarItems = starItems.filter(item => item !== id);
            this.setState({ starItems: newStarItems }, () => {
                localStorage.setItem('starItems', JSON.stringify(newStarItems));
            });
        } else {
            const newStarItems = [...starItems, id];
            this.setState({ starItems: newStarItems }, () => {
                localStorage.setItem('starItems', JSON.stringify(newStarItems));
            });
        }
    }

    handleBucketClick = (id) =>{
        const {bucketItems} = this.state
        const newBucketItems = [...bucketItems, +id]
        this.setState({ bucketItems: newBucketItems }, () => {
            localStorage.setItem('bucketItems', JSON.stringify(newBucketItems));
        });
    }


    openModal = (e) => {
        const modalID = e.target.getAttribute('data-modal')
        const modalDeclaration = this.getModalInfo().find(item => item.modalId === modalID);
        this.setState({
            showModal: true,
            activeModal: modalDeclaration,
            activeModalCardId: e.target.id
        })
    }

    closeModal = () => {
        this.setState(prevState => ({showModal: !prevState.showModal}));
    }

    render() {
        const {productInfo, loading, activeModal, starItems, bucketItems} = this.state;
        return (
            <>
                <Header starItems={starItems} bucketItems={bucketItems}></Header>
                {loading ? (<div>Loading...</div>) : (
                    <ProductList
                        productInfo={productInfo}
                        handleStarClick={this.handleStarClick}
                        handleBucketClick={this.handleBucketClick}
                        starItems={starItems}
                        bucketItems={bucketItems}
                        openModal={this.openModal} />)}
                {this.state.showModal ?
                    <Modal
                        data={activeModal}
                        closeModal={this.closeModal}
                        activeModalCardId={this.state.activeModalCardId}
                        handleBucketClick={this.handleBucketClick}
                    /> : null}
            </>
        )
    }
}

export default App;
