import {Component} from "react";

class GetProduct extends Component{
    async getProductInfo () {
        const res = await fetch('productCollection.json')
        const result = await res.json()
        return result
    }
}

export default GetProduct