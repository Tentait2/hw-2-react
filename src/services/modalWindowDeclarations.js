const modalWindowDeclarations = [
    {
        modalId: 'modalID1',
        header: 'Do you want to add this product to backet?',
        modalText: 'Are you sure you want to do it?',
        backgroundColor: '#c7c7c7',
        closeButton : true
    },
    {
        modalId: 'modalID2',
        header: 'You are in the good mood?',
        modalText: 'I hope you are in the good mood!',
        backgroundColor: '#98ffe8',
        closeButton : false,
    }
]

export default modalWindowDeclarations