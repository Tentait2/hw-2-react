import {Component} from "react";
import './Modal.scss'
import img from '../../resourses/img/cross.svg'
import PropTypes from "prop-types";
class Modal extends Component{
    constructor(props) {
        super(props);

    }
    submitBucketModal = (e) =>{
        const {activeModalCardId, handleBucketClick} = this.props
        this.props.closeModal({showModal:false})
        handleBucketClick(activeModalCardId)
    }
    closeModal = (e) =>{
        if (e.target.className === 'background' || e.target.className ==='close-modal' || e.target.value === 'closeModal'){
            this.props.closeModal({showModal:false})
        }
    }
    render() {
        const {id, header, modalText, backgroundColor, closeButton} = this.props.data
        return (
            <div className={'background'} onClick={this.closeModal} >
                <div key={id} className={'modal'} style={{backgroundColor}}>
                    <h1>{header}</h1>
                    <p>{modalText}</p>
                    {closeButton ? <img className={'close-modal'} src={img} alt="#"/> : null}
                    <button className={'button-modal'} key={1}
                            onClick={this.submitBucketModal}
                    >Yes</button> <button className={'button-modal'} value={'closeModal'} key={2}>No</button>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    backgroundColor : PropTypes.string,
    modalText: PropTypes.string,
    closeButton: PropTypes.func,
    id: PropTypes.number,
};

export default Modal