import {Component} from "react";

import "./Header.scss"
import logo from '../../resourses/img/Jumpman_logo.svg.png'
import favouriteImg from '../../resourses/img/favourite-icon.png'
import cartIcon from '../../resourses/img/cart-icon.png'
import PropTypes from "prop-types";
import Button from "../button/Button";

class Header extends Component{
    constructor(props) {
        super(props);

    }
    render() {
        const {starItems, bucketItems} = this.props

        return (
            <div className={'page-header'}>
                <img className={'page-logo'} src={logo} alt={'logo'}></img>
                <h1 className={'page-title'}>Jordan Shop</h1>
                <div className={'header-icons-wrapper'}>
                    <img className={'header-icons'} src={cartIcon} alt="cartIcon"/>
                    <p className={'cart-length'}>{bucketItems.length}</p>
                    <img className={'header-icons'} src={favouriteImg} alt="cartIcon"/>
                    <p className={'favourite-length'}>{starItems.length}</p>
                </div>
            </div>
        )
    }
}

Header.propTypes = {
    starItems: PropTypes.array,
    bucketItems: PropTypes.array
};

export default Header