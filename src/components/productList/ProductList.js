import {Component} from "react";
import PropTypes from "prop-types";

import './ProductList.scss'
import ProductCard from "../productCard/ProductCard";

class ProductList extends Component{
    constructor(props) {
        super(props);
    }


    getProducts = () => {
        const {productInfo, handleStarClick, handleBucketClick, openModal} = this.props
        const elements = productInfo.map((item) => {
            const {...itemProps} = item
            return (<ProductCard
                starItems={this.props.starItems}
                bucketItems = {this.props.bucketItems}
                handleStarClick = {handleStarClick}
                handleBucketClick={handleBucketClick}
                key={itemProps.id}
                openModal={openModal}
                {...itemProps}/>)
        })
        return elements
    }

    render() {
        return (
            <ul className="card-list">
                {this.getProducts()}
            </ul>
        )
    }
}



ProductList.defaultProps = {
    name: "Not in stock",
    price: 'sold out',
};


export default ProductList