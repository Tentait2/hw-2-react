import {Component} from "react";
import './ProductCard.scss'
import Button from "../button/Button";
import PropTypes from "prop-types";
class ProductCard extends Component{
    constructor(props) {
        super(props);
    }

    state = {
        starredStar: false,
        starredBucket: false,
    }
    componentDidMount() {
        const {id, starItems, bucketItems} = this.props
        if (starItems.includes(id)) {
            this.setState({ starredStar: true });
        }
        if (bucketItems.includes(id)) {
            this.setState({starredBucket : true });
        }
    }
    componentDidUpdate(prevProps) {
        const { id, bucketItems } = this.props;
        if (prevProps.bucketItems !== bucketItems) {
            const isIdInBucket = bucketItems.includes(id);
            if (isIdInBucket) {
                this.setState({ starredBucket: true });
            } else {
                this.setState({ starredBucket: false });
            }
        }
    }


    handleStarClick = (id) =>{
        const {handleStarClick} = this.props
        this.setState(prevState => ({starredStar : !prevState.starredStar}))
        handleStarClick(id)
    }

    handleBusterClick = (id) =>{
        const {handleBucketClick} = this.props
        handleBucketClick(id)
    }



    render() {
        const {id, name, price, imgUrl, vendorCode, color, openModal} = this.props
        return (
            <li className={'card-list-item'}>
                <img className={'list-img'} src={imgUrl} alt="item-img"/>
                <p className={'list-item-text'}>{name}</p>
                <p className={'list-item-text'}>{price} UAH</p>
                <p className={'list-item-text'}>{vendorCode}</p>
                <p className={'list-item-text'}> {color}</p>
                <p className={'item-favourite'} onClick={() => this.handleStarClick(id)}>{this.state.starredStar ? '★' : '☆'}</p>
                {this.state.starredBucket ? null : <Button id={id} data = {'modalID1'} backgroundColor={'grey'} text={`Add to cart`} handleBusterClick={this.handleBusterClick} openModal={openModal}/>}
            </li>
        )
    }
}




export default ProductCard